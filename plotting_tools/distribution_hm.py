#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed 7 Feb 2018

@author: tsuchida
"""

import matplotlib.pyplot as plt
import os
import os.path as op
import pandas as pd
import seaborn as sns
from .utils import get_histmat_df


def plot_hist_hm(sub_list, im_list, bin_num=100, batch_size=50, density=False, out_bn=None):
    
    # Get histmat_df from im_list
    idx = pd.Index(sub_list, name="Subject ID")
    histmat_df = get_histmat_df(idx, im_list, bin_num=bin_num, density=density)
    
    # Use values within median +/- 3 IQR for the following
    # Get min and max value for heatmap
    (vmin, vmax) = 0, histmat_df.iloc[:, 1:-1].values.max()
    # Get argmax per row (subj) to sort subj  
    histmat_df['argmax'] = histmat_df.iloc[:, 1:-1].idxmax(axis=1)
    sorted_df = histmat_df.sort_values(by=["argmax"])

    # Now plot and save
    # Setting colorbar title
    cbar_label = "% voxel" if density else "voxel count"
    # If sublist is longer than batch_size, break up the plots
    if len(sub_list) > batch_size:
        batch_num = len(sub_list) / batch_size
        for i in range(batch_num + 1):
            if i==batch_num:
                batch_sublist = sub_list[(i*batch_size):]
                batch_df = sorted_df.iloc[(i*batch_size):, :-1]
            else:
                batch_sublist = sub_list[(i*batch_size): ((i+1)*batch_size)]
                batch_df = sorted_df.iloc[(i*batch_size): ((i+1)*batch_size), :-1]
            
            fig, ax = plt.subplots(figsize=(12, 0.2*len(batch_sublist)))
            sns.heatmap(batch_df, cmap="YlGnBu", xticklabels=10, 
                        cbar_kws={'label': cbar_label},
                        vmin=vmin, vmax=vmax, ax=ax)
            fig.tight_layout()
            
            if out_bn is not None:
                if not op.exists(out_bn):
                    os.makedirs(out_bn)
                fig.savefig(op.join(out_bn, 'batch_%02d.png' % i))
    
    else:
        fig, ax = plt.subplots(figsize=(12, 0.2*len(sub_list)))
        sns.heatmap(sorted_df.iloc[:, :-1], cmap="YlGnBu", xticklabels=10,
                    cbar_kws={'label': cbar_label},
                    vmin=vmin, vmax=vmax, ax=ax)
        fig.tight_layout()
        
        if out_bn is not None:
            fig.savefig('%s.png' % out_bn, bbox_inches='tight')

    return fig
