#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 6 2018

@author: tsuchida
"""

import math
import os.path as op
import numpy as np
import nibabel as nib
import matplotlib.pyplot as plt
from mriqc.viz.utils import _get_limits, plot_slice


def plot_sag_perframe(img, out_file=None, ncols=8, title=None, overlay_mask=None,
                bbox_mask_file=None, flabels=None,
                vmin=None, vmax=None, cmap='Greys_r', fig=None):


    if isinstance(img, (str, bytes)):
        nii = nib.as_closest_canonical(nib.load(img))
        img_data = np.asanyarray(nii.dataobj)
        zooms = nii.header.get_zooms()
    else:
        img_data = img
        zooms = [1.0, 1.0, 1.0]
        out_file = 'mosaic.svg'

    # Remove extra dimensions
    img_data = np.squeeze(img_data)

    # Reshape if only one frame
    if len(img_data.shape) == 3:
        img_data = img_data.reshape((img_data.shape)+(1,))

    # Get number of frames to plot
    nframes = img_data.shape[-1]

    if flabels is not None:
        assert len(flabels) == nframes, "Number of frame labels do not much the number of frames of the image"

    nrows = math.ceil(nframes / float(ncols))

    if overlay_mask:
        overlay_data = nib.as_closest_canonical(
            nib.load(overlay_mask)).get_data()

    # create figures
    if fig is None:
        fig = plt.figure(figsize=(22, nrows * 2.5))

    est_vmin, est_vmax = _get_limits(img_data,
                                     only_plot_noise=False)
    if not vmin:
        vmin = est_vmin
    if not vmax:
        vmax = est_vmax

    naxis = 1

    # Get mid-saggital x
    mid_x = int(img_data.shape[0] / 2.0)
    for frame in range(nframes):
        ax = fig.add_subplot(nrows, ncols, naxis)

        if overlay_mask:
            ax.set_rasterized(True)
        plot_slice(img_data[mid_x, :, :, frame], vmin=vmin, vmax=vmax,
                   cmap=cmap, ax=ax, spacing=[zooms[0], zooms[2]],
                   label='frame %d' % (frame if flabels is None else flabels[frame]))

        if overlay_mask:
            from matplotlib import cm
            msk_cmap = cm.Reds  # @UndefinedVariable
            msk_cmap._init()
            alphas = np.linspace(0, 0.75, msk_cmap.N + 3)
            msk_cmap._lut[:, -1] = alphas
            plot_slice(overlay_data[mid_x, :, :], vmin=0, vmax=1,
                       cmap=msk_cmap, ax=ax, spacing=[zooms[0], zooms[2]])
        naxis += 1

    fig.subplots_adjust(
        left=0.05, right=0.95, bottom=0.05, top=0.95, wspace=0.05,
        hspace=0.05)

    if title:
        fig.suptitle(title, fontsize='10')
    fig.subplots_adjust(wspace=0.002, hspace=0.002)

    if out_file is None:
        fname, ext = op.splitext(op.basename(img))
        if ext == ".gz":
            fname, _ = op.splitext(fname)
        out_file = op.abspath(fname + '_mosaic.svg')

    fig.savefig(out_file, format='svg', dpi=300, bbox_inches='tight')
    return out_file


def plot_multi_surf_stat(lh_surf, rh_surf,
                         lh_bg_map, rh_bg_map,
                         lh_stat_maps, rh_stat_maps, 
                         group_names, out_fname, fig_title='',
                         cmap='coolwarm', symmetric_cbar='auto',
                         upper_lim=None, thresholds=None):
    '''
    Use Nilearn to plot statistical surface map for lat, med, sup, inf views
    for both hemispheres for multiple groups using the same threshold.
    '''
    import os.path as op
    import numpy as np
    import matplotlib as mpl
    mpl.use('Agg')
    import matplotlib.pyplot as plt
    import nibabel.freesurfer.io as fsio
    import nibabel.freesurfer.mghformat as fsmgh
    from nilearn import plotting
    
    # Make sure list of maps and group names have same length
    assert len(group_names) == len(lh_stat_maps)
    assert len(group_names) == len(rh_stat_maps)
    
    # if threshold is defined, make sure they are right size and get values
    if thresholds is not None:
        assert len(group_names) == len(thresholds)
        
        thresh_vals = []
        for threshold in thresholds:
            if isinstance(threshold, str):
                if op.exists(threshold):
                    thresh_arr = np.loadtxt(threshold)
                    thresh = thresh_arr if thresh_arr.shape == () and thresh_arr != np.inf else None
                else:
                    thresh = None
    
            elif isinstance(threshold, int) or isinstance(threshold, float):
                thresh = threshold
        
            else:
                thresh = None
                
            thresh_vals.append(thresh)
            
    
    # Get max and min value across all the stat maps 
    lh_stats = [fsmgh.load(lh_map).get_data() for lh_map in lh_stat_maps]
    rh_stats = [fsmgh.load(rh_map).get_data() for rh_map in rh_stat_maps]
   
    flat_dat = np.hstack([dat.flatten() for dat in lh_stats + rh_stats])
    max_val = np.maximum(np.abs(np.nanmax(flat_dat)), np.abs(np.nanmin(flat_dat)))
    if upper_lim is not None:
        vmax = upper_lim if max_val > upper_lim else max_val
    else:
        vmax = max_val
    
    n_rows = 4
    n_cols = 2*len(group_names)
    fig, axs = plt.subplots(n_rows, n_cols,
                            figsize=(4*len(group_names), 6),
                            subplot_kw={'projection': '3d'})
    
    # Add title if provided
    if fig_title:
        fig.suptitle(fig_title, fontsize=16)
 
    group_width = 0.9 / len(group_names)
    for i, (group_name, lh_map, rh_map) in enumerate(zip(group_names, lh_stats, rh_stats)):
        # Add group title
        group_x = 0.05 + (i * group_width) + (group_width / 2.0)
        plt.figtext(group_x, 0.9, group_name,
                    horizontalalignment='center', fontsize=12)
        
        # Add threshold to title if not None
        title_txt = ''
        thresh = None
        if thresholds is not None:
            thresh = thresh_vals[i]
            if thresh is not None:
                if float(thresh) >= 1e-2 and float(thresh) < 1e2:
                    title_txt = 'Threshold = {:.2f}'.format(float(thresh))
                else:
                    title_txt = 'Threshold = {:.2e}'.format(float(thresh))

        # Plot 
        col = (i * 2)
        hemis = ('left', 'right')
        views = ('lateral', 'medial', 'dorsal', 'ventral')
        for hemi in hemis:
            if hemi == 'left':
                surf = lh_surf
                bg = lh_bg_map
                stat_map = lh_map
                col = (i * 2)
            else:
                surf = rh_surf
                bg = rh_bg_map
                stat_map = rh_map
                col = (i * 2) + 1
                
            for row, view in enumerate(views):
                title = title_txt if row == 0 and hemi == 'left' else ''
                if thresholds is not None: # plot color bar for every group
                    colorbar = True if row == 3 and hemi == 'right' else False
                else: # only plot one color bar 
                    colorbar = True if row == 3 and hemi == 'right' and (i+1) == len(group_names) else False
                    
                plotting.plot_surf_stat_map(surf,
                                            stat_map,
                                            hemi=hemi,
                                            bg_map=bg,
                                            view=view,
                                            vmax=vmax,
                                            threshold=thresh,
                                            title=title,
                                            cmap=cmap,
                                            symmetric_cbar=symmetric_cbar,
                                            colorbar=colorbar,
                                            axes=axs[row, col],
                                            figure=fig)
                

    
    fig.savefig(out_fname, dpi=300, bbox_inches='tight')
    
    return op.abspath(out_fname)


def plot_multi_surf_map(lh_surf, rh_surf,
                        lh_bg_map, rh_bg_map,
                        lh_surf_maps, rh_surf_maps, 
                        group_names, out_fname, fig_title='',
                        cmap='jet', vmin=None, vmax=None):
    '''
    Use Nilearn to plot surface map for lat, med, sup, inf views
    for both hemispheres for multiple groups using the same scale.
    '''
    import os.path as op
    import numpy as np
    import matplotlib as mpl
    mpl.use('Agg')
    import matplotlib.pyplot as plt
    import nibabel.freesurfer.io as fsio
    import nibabel.freesurfer.mghformat as fsmgh
    from nilearn import plotting
    
    # Make sure list of maps and group names have same length
    assert len(group_names) == len(lh_surf_maps)
    assert len(group_names) == len(lh_surf_maps)
    
    
    # Get max and min value across all the stat maps 
    lh_maps = [fsmgh.load(lh_map).get_data() for lh_map in lh_surf_maps]
    rh_maps = [fsmgh.load(rh_map).get_data() for rh_map in rh_surf_maps]
    
    if vmin is None or vmax is None:
        flat_dat = np.hstack([dat.flatten() for dat in lh_maps + rh_maps])
        
    if vmin is None:
        vmin = np.nanmin(flat_dat)
    if vmax is None:
        vmax = np.nanmax(flat_dat)
    
    n_rows = 4
    n_cols = 2*len(group_names)
    fig, axs = plt.subplots(n_rows, n_cols,
                            figsize=(4*len(group_names), 6),
                            subplot_kw={'projection': '3d'})
    
    # Add title if provided
    if fig_title:
        fig.suptitle(fig_title, fontsize=16)
 
    group_width = 0.9 / len(group_names)
    for i, (group_name, lh_map, rh_map) in enumerate(zip(group_names, lh_maps, rh_maps)):
        print('group: {}'.format(group_name))
        # Add group title
        group_x = 0.05 + (i * group_width) + (group_width / 2.0)
        plt.figtext(group_x, 0.9, group_name,
                    horizontalalignment='center', fontsize=12)
        
        # Plot 
        hemis = ('left', 'right')
        views = ('lateral', 'medial', 'dorsal', 'ventral')
        for hemi in hemis:
            if hemi == 'left':
                surf = lh_surf
                bg = lh_bg_map
                surf_map = lh_map
                col = (i * 2)
            else:
                surf = rh_surf
                bg = rh_bg_map
                surf_map = rh_map
                col = (i * 2) + 1
                
            for row, view in enumerate(views):
                colorbar = True if view == 'ventral' and hemi == 'right' and (i+1) == len(group_names) else False
                print('ax: {}, {}'.format(row, col))
                print('view: {}'.format(view))
                print('hemi: {}'.format(hemi))
                print('colorbar: {}'.format(colorbar))
                
                plotting.plot_surf(surf,
                                   surf_map,
                                   hemi=hemi,
                                   bg_map=bg,
                                   view=view,
                                   vmin=vmin,
                                   vmax=vmax,
                                   cmap=cmap,
                                   colorbar=colorbar,
                                   axes=axs[row, col],
                                   figure=fig)

    fig.savefig(out_fname, dpi=300, bbox_inches='tight')
    
    return op.abspath(out_fname)