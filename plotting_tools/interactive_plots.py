#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 17:07:07 2018

@author: tsuchida
"""

import numpy as np
from scipy import stats
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import bokeh.io
import bokeh.models
import bokeh.palettes
import bokeh.plotting
from bokeh.layouts import widgetbox, column, row
from bokeh.transform import jitter
from bokeh.models import (ColumnDataSource, HoverTool, GlyphRenderer,
                          CategoricalColorMapper, CustomJS, Label, Range1d)
from bokeh.models.glyphs import HBar, VBar, Circle
from bokeh.models.widgets import DataTable, TableColumn, StringFormatter

from .utils import stack_df, get_bokehpalette, get_lims, str_format_val
from .stats import orthoregress


def plot_histogram(summary_df, measure_name,
                   col_groupname, cols_to_plot, colnames=None,
                   title=None, num_bins=100, bin_range=None,
                   cmap="Spectral", palette=None,
                   plot_size=(800, 500), out_html=None):

    # We only need cols_to_plot
    df_toplot = summary_df[cols_to_plot]

    # default title
    if title is None:
        title = '%s histogram by %s' % (measure_name, col_groupname)

    p = bokeh.plotting.figure(plot_width=plot_size[0],
                              plot_height=plot_size[1],
                              title=title)

    # Get histogram edges for the whole dataset if range is not provided
    if bin_range is None:
        hist, edges = np.histogram(df_toplot.dropna(), bins=num_bins)
    else:
        edges = np.linspace(bin_range[0], bin_range[1], num=num_bins, endpoint=False)
        np.append(edges, bin_range[1])
        p.x_range = Range1d(bin_range[0], bin_range[1])

    # Compute histogram values for each column in cols_to_plot and keep them in a df
    hist_df = pd.DataFrame({'bin_l': edges[:-1], 'bin_u': edges[1:]})
    for col in cols_to_plot:
        hist_df[col] = np.histogram(df_toplot[col].dropna(), bins=edges)[0]

    # Stack them to create a source
    stacked = stack_df(hist_df, cols_to_plot, col_groupname, 'count')
    source = ColumnDataSource(stacked)

    # Categorical color mapper
    # If palette is provided, it overrides cmap
    if palette is not None:
        my_palette = palette
    else:
        my_palette = get_bokehpalette(cmap, len(cols_to_plot))

    color_mapper = CategoricalColorMapper(
        factors=cols_to_plot, palette=my_palette)

    # Tooltips
    tooltips = [('%s bin' % measure_name, '%s - %s' %('@bin_l', '@bin_u')),
                ('%s' % col_groupname, '@%s' % col_groupname),
                ('count', '@count')]
    hover = HoverTool(tooltips=tooltips)
    p.add_tools(hover)

    # plot histograms
    p.quad(top='count', bottom=0, left='bin_l', right='bin_u',
           fill_color={'field': col_groupname, 'transform': color_mapper},
           line_color='slategray', alpha=0.6,
           hover_fill_color={'field': col_groupname, 'transform': color_mapper},
           hover_line_color='black', hover_alpha=1.0,
           legend=col_groupname, source=source)

    # Save as html
    if out_html is not None:
        bokeh.io.save(p, filename=out_html, title=title)
    return p


def plot_dist_by_cols(summary_df, measure_name,
                      col_groupname, cols_to_plot, colnames=None,
                      title=None, cmap="Spectral", palette=None,
                      vertical=True,
                      plot_size=None, out_html=None):

    # We only need cols_to_plot
    # Note that it uses idx values and name to label scatter points
    df_toplot = summary_df[cols_to_plot]

    # default title
    if title is None:
        title = '%s values by %s' % (measure_name, col_groupname)

    # plot top to bottom if vertical
    if vertical:
        y_range = cols_to_plot[::-1]
        if plot_size is None:
            plot_size = (800, 40*len(y_range))
        p = bokeh.plotting.figure(plot_width=plot_size[0], plot_height=plot_size[1],
                                  y_range=y_range, title=title)

    else:
        x_range = cols_to_plot
        if plot_size is None:
            plot_size = (40*len(x_range), 800)
        p = bokeh.plotting.figure(plot_width=plot_size[0], plot_height=plot_size[1],
                                  x_range=x_range, title=title)

    # Stack them to create a source
    stacked = stack_df(df_toplot, cols_to_plot, col_groupname, measure_name)
    source = ColumnDataSource(stacked)

    # Categorical color mapper
    # If palette is provided, it overrides cmap
    if palette is not None:
        my_palette = palette
    else:
        my_palette = get_bokehpalette(cmap, len(cols_to_plot))

    color_mapper = CategoricalColorMapper(
        factors=cols_to_plot, palette=my_palette)

    # Make and add the hover tool
    tooltips = [(df_toplot.index.name, '@%s' % df_toplot.index.name),
                (col_groupname, '@%s' % col_groupname),
                ('%s value' % measure_name, '@%s' % measure_name)]
    # link selection by index.name
    #callback = CustomJS(args={'source': source},
    #                    code="source.set('selected', source.data['%s']);" % df_toplot.index.name)
    #hover = HoverTool(tooltips=tooltips, callback=callback)
    hover = HoverTool(tooltips=tooltips)
    p.add_tools(hover)
    p.xaxis.axis_label = "%s value" % measure_name

    # plot scatter points
    if vertical:
        x = measure_name
        y = jitter(col_groupname, width=0.6, range=p.y_range)
    else:
        x = jitter(col_groupname, width=0.6, range=p.x_range)
        y = measure_name
    p.circle(x=x, y=y,
             source=source, size=8,
             fill_color={'field': col_groupname, 'transform': color_mapper},
             hover_fill_color='slategray', fill_alpha=0.6,
             line_color='slategray', hover_line_color="black")

    p.min_border=10
    if out_html is not None:
        bokeh.io.save(p, filename=out_html, title=title)
    return p


def plot_dist_box_by_cols(summary_df, measure_name,
                        col_groupname, cols_to_plot, colnames=None,
                        title=None, cmap="Spectral", palette=None,
                        horizontal=True,plot_range=None,
                        plot_size=None, out_html=None):

    # We only need cols_to_plot
    # Note that it uses idx values and name to label scatter points
    df_toplot = summary_df[cols_to_plot]

    # default title
    if title is None:
        title = '%s by %s' % (measure_name, col_groupname)

    # Stack them to prepare for plotting
    stacked = stack_df(df_toplot, cols_to_plot, col_groupname, measure_name)

    # find the quartiles and IQR for each category
    groups = stacked.groupby(col_groupname)
    mean = groups.mean()
    count = groups.count()
    q1 = groups.quantile(q=0.25)
    q2 = groups.quantile(q=0.5)
    q3 = groups.quantile(q=0.75)
    iqr = q3 - q1
    upper = q3 + 1.5*iqr
    lower = q1 - 1.5*iqr

    # Put them in a single df to create a datasource for box plots
    box_source = pd.concat([mean, count, q1, q2, q3, iqr, upper, lower], axis=1)
    box_source.columns = ['mean', 'count', '25p', '50p', '75p', 'IQR', 'upper', 'lower']
    box_source = ColumnDataSource(box_source)

    # find the outliers for each category
    def outliers(group):
        cat = group.name
        return group[(group[measure_name] > upper.loc[cat][measure_name]) | (group[measure_name] < lower.loc[cat][measure_name])][measure_name]

    out = groups.apply(outliers).dropna()

    # if there are outliers, put them in a single df to create a datasource for
    # outlier scatter plots
    if not out.empty:
        out_dat = {}
        for cat in cols_to_plot:
            if cat in out.index.unique(level=0):
                num_out = len(out.loc[cat])
                out_dat[col_groupname] = out_dat.get(col_groupname, []) + [cat]*num_out
                out_dat[out.loc[cat].index.name] = out_dat.get(out.loc[cat].index.name, []) + (out.loc[cat].index.tolist())
                out_dat[measure_name] = out_dat.get(measure_name, []) + (out.loc[cat].values.tolist())

        out_source = ColumnDataSource(pd.DataFrame(out_dat))

    # Categorical color mapper
    # If palette is provided, it overrides cmap
    if palette is not None:
        my_palette = palette
    else:
        my_palette = get_bokehpalette(cmap, len(cols_to_plot))

    color_mapper = CategoricalColorMapper(
        factors=cols_to_plot, palette=my_palette)

    # properties for color etc.
    style_d = dict(fill_color={'field': col_groupname, 'transform': color_mapper},
                   fill_alpha=0.5, line_color='slategray')

    hover_style_d = dict(fill_color={'field': col_groupname, 'transform': color_mapper},
                        fill_alpha=0.9, line_color='black')
    
    # Set the plot range on the measure dimension if plot_range is not given
    if plot_range is None:
        plot_range = get_lims(stacked[measure_name].values)
    measure_range = Range1d(plot_range[0], plot_range[1])
    very_thin_range = 0.001*(plot_range[1] - plot_range[0])
     
    # plot top to bottom if horizontal
    if horizontal:
        y_range = cols_to_plot[::-1]
        if plot_size is None:
            plot_size = (800, 80+50*len(y_range))
        p = bokeh.plotting.figure(plot_width=plot_size[0], plot_height=plot_size[1],
                                  x_range=measure_range, y_range=y_range, title=title)
        
        # components for vertically arranged horizontal boxplots
        seg1_d = dict(x0='25p', y0=col_groupname, x1='lower', y1=col_groupname)
        seg2_d = dict(x0='75p', y0=col_groupname, x1='upper', y1=col_groupname)
        box = HBar(y=col_groupname, right='75p', left='25p', height=0.5,
                   line_width=2, **style_d)
        box_h = HBar(y=col_groupname, right='75p', left='25p', height=0.5,
                     line_width=2, **hover_style_d)
        whisk1_d = dict(x='lower', y=col_groupname, width=very_thin_range, height=0.2)
        whisk2_d = dict(x='upper', y=col_groupname, width=very_thin_range, height=0.2)
        whisk3_d = dict(x='mean', y=col_groupname, width=very_thin_range, height=0.5)
        whisk4_d = dict(x='50p', y=col_groupname, width=very_thin_range, height=0.5)
        scatter = Circle(x=measure_name, size=8,
                         y=jitter(col_groupname, width=0.6, range=p.y_range),
                         **style_d)
        scatter_h = Circle(x=measure_name, size=8,
                         y=jitter(col_groupname, width=0.6, range=p.y_range),
                         **hover_style_d)
        p.xaxis.axis_label = "%s" % measure_name

    else:
        x_range = cols_to_plot
        if plot_size is None:
            plot_size = (80+50*len(x_range), 800)
        p = bokeh.plotting.figure(plot_width=plot_size[0], plot_height=plot_size[1],
                                  x_range=x_range, y_range=measure_range, title=title)

        # components for horizontally arranged vertical boxplots
        seg1_d = dict(x0=col_groupname, y0='25p', x1=col_groupname, y1='lower')
        seg2_d = dict(x0=col_groupname, y0='75p', x1=col_groupname, y1='upper')
        box = VBar(x=col_groupname, top='75p', bottom='25p', width=0.5,
                   line_width=2, **style_d)
        box_h = VBar(x=col_groupname, top='75p', bottom='25p', width=0.5,
                     line_width=2, **hover_style_d)
        whisk1_d = dict(x=col_groupname, y='lower', width=0.2, height=very_thin_range)
        whisk2_d = dict(x=col_groupname, y='upper', width=0.2, height=very_thin_range)
        whisk3_d = dict(x=col_groupname, y='mean', width=0.5, height=very_thin_range)
        whisk4_d = dict(x=col_groupname, y='50p', width=0.5, height=very_thin_range)
        scatter = Circle(x=jitter(col_groupname, width=0.6, range=p.y_range),
                         y=measure_name, size=8,
                         **style_d)
        scatter_h = Circle(x=jitter(col_groupname, width=0.6, range=p.y_range),
                         y=measure_name, size=8,
                         **hover_style_d)
        p.yaxis.axis_label = "%s" % measure_name

    # Now plot the box by each component
    p.segment(line_color='slategray', line_width=4, source=box_source, **seg1_d)
    p.segment(line_color='slategray', line_width=4, source=box_source, **seg2_d)
    p.rect(line_color='slategray', line_width=4, source=box_source, **whisk1_d)
    p.rect(line_color='slategray', line_width=4, source=box_source, **whisk2_d)
    p.rect(line_color='slategray', source=box_source, **whisk3_d)
    p.rect(line_color='slategray', line_width=4, source=box_source, **whisk4_d)
    box_glyph = GlyphRenderer(data_source=box_source, glyph=box, hover_glyph=box_h)

    # Make and add the hover tool
    box_tooltips = [(col_groupname, '@%s' % col_groupname),
                    ('count', '@count'),
                    ('mean', '@mean'),
                    ('median', '@50p'),
                    ('IQR', '@IQR'),
                    ('upper', '@upper'),
                    ('lower', '@lower')]
    box_hover = HoverTool(renderers=[box_glyph], tooltips=box_tooltips)
    p.add_tools(box_hover)
    p.renderers.extend([box_glyph])

    # Plot outliers if any
    if not out.empty:
        out_scatter = GlyphRenderer(data_source=out_source, glyph=scatter, hover_glyph=scatter_h)
        scatter_tooltips = [(df_toplot.index.name, '@%s' % df_toplot.index.name),
                            (col_groupname, '@%s' % col_groupname),
                            ('%s value' % measure_name, '@%s' % measure_name)]
        scat_hover = HoverTool(renderers=[out_scatter], tooltips=scatter_tooltips)
        p.add_tools(scat_hover)
        p.renderers.extend([out_scatter])

    p.min_border=10
    if out_html is not None:
        bokeh.io.save(p, filename=out_html, title=title)
    return p


def make_summary_table(summary_df, measure_name,
                       col_groupname, cols_to_plot, colnames=None,
                       summary_strform_func=(lambda x: '%.3f' % x),
                       table_size=(800, 280)):

    df = summary_df[cols_to_plot].apply(pd.to_numeric)
    stat_summary=df.describe()
    stat_summary.reset_index(level=0, inplace=True)
    stat_summary.columns = ['summary'] + cols_to_plot

    # Format summary:
    stat_summary.iloc[1:, 1:] = stat_summary.iloc[1:, 1:].applymap(summary_strform_func)

    source = ColumnDataSource(stat_summary)

    columns = [TableColumn(field="summary", title="summary", formatter=StringFormatter())] + \
              [TableColumn(field=col, title=name) for (col, name) in zip(cols_to_plot, colnames)]

    data_table = DataTable(source=source, columns=columns,
                           width=table_size[0], height=table_size[1])

    return widgetbox(data_table)


def plot_distributions(summary_df, dti_measure, out_html=None):

    title = '%s Distributions' % dti_measure
    p1 = plot_histogram(summary_df, dti_measure)
    p2 = plot_dist_by_regions(summary_df, dti_measure)
    w = make_summary_table(summary_df, dti_measure)
    plots = column(p1, p2, w)
    # Save as html
    if out_html is not None:
        bokeh.io.save(plots, filename=out_html, title=title)
    return plots


def plot_hist_box(summary_df, measure_name,
                 col_groupname, cols_to_plot,
                 title=None, hist_bins=100, plot_range=None,
                 plot_width=None,
                 palette=None, out_html=None):
    
    # Set common range
    if plot_range is None:
        plot_range = get_lims(summary_df[cols_to_plot].values.flatten())
    # default title
    if title is None:
        title = '%s distributions by %s' % (measure_name, col_groupname)
    # set proper plot size if plot_width is not None
    if plot_width is not None:
        hist_size = (plot_width, int(round(plot_width*(5.0/8.0), 0)))
        distbox_size = (plot_width, 80+50*len(cols_to_plot))
    else:
        hist_size = (800, 500)
        distbox_size = None
    p1 = plot_histogram(summary_df, measure_name, col_groupname, cols_to_plot,
                        num_bins=hist_bins, bin_range=plot_range, title=title,
                        palette=palette, plot_size=hist_size)
    p2 = plot_dist_box_by_cols(summary_df, measure_name, col_groupname, cols_to_plot,
                               plot_range=plot_range, title='', palette=palette,
                               plot_size=distbox_size)
    plots = column(p1, p2)
    # Save as html
    if out_html is not None:
        bokeh.io.save(plots, filename=out_html, title=title)
        
    # Save as png...this did not work for me
#    if out_png is not None:
#        bokeh.io.export_png(plots, filename=out_png)
    return plots


def pairplots_by_region(summary_df, measure_name,
                        col1, col2, colnames=None,
                        title=None, color='navy', plot_size=(500, 500),
                        line_fit='ortho', out_html=None):

    # We only need cols_to_plot
    # Note that it uses idx values and name to label scatter points
    df_toplot = summary_df[[col1, col2]]

    # default title
    if title is None:
        title = 'Comparison of %s values between %s and %s' \
                % (measure_name, col1, col2)

    # get lims from range of values in col1 and col2
    lims = get_lims(df_toplot.values)
    p = bokeh.plotting.figure(plot_width=plot_size[0], plot_height=plot_size[1],
                              x_range=lims, y_range=lims,
                              title=title)

    # Add a diagnal line in the background
    p.line(lims, lims, line_width=2, line_color='slategray',
           line_alpha=0.5, line_dash='dashed')
    source = bokeh.models.ColumnDataSource(df_toplot)
    points=p.circle(x=col1, y=col2, source=source,
                    size=10, fill_color=color,
                    hover_fill_color="firebrick",
                    fill_alpha=0.2, hover_alpha=0.5,
                    line_color='slategray', hover_line_color="firebrick")
    # Make and add the hover tool
    tooltips = [(df_toplot.index.name, '@%s' % df_toplot.index.name),
                ('%s' % col1, '@%s' % col1),
                ('%s' % col2, '@%s' % col2)]
    hover = bokeh.models.HoverTool(tooltips=tooltips, renderers=[points])
    p.add_tools(hover)
    p.xaxis.axis_label = col1
    p.yaxis.axis_label = col2

    # Add line fit if line_fit is not None
    if line_fit is not None:
        # Drop na from data
        nonan_dat = df_toplot.dropna()
        x, y = nonan_dat[col1].values, nonan_dat[col2].values
        # Pearson correlation
        corr, corr_p = stats.pearsonr(x, y)
        delta = (lims[1] - lims[0])*0.05
        x_0 = lims[0] + delta
        x_1 = lims[1] - delta
        
        if line_fit == 'lin':  
            slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
            txt2 = 'Rsquare=%0.3f' % (r_value**2)
        elif line_fit == 'ortho':
            slope, intercept, res = orthoregress(x, y) 
            txt2 = 'Residual Var=%0.3f' % (res)

        y_0 = slope * x_0 + intercept
        y_1 = slope * x_1 + intercept
        
        # Add line
        p.line([x_0, x_1], [y_0, y_1], line_width=1, line_color='firebrick')
        # Add txt
        if intercept >= 0:
            equation_txt = 'Y=%0.3f*X+%0.3e' % (slope, intercept) 
        else:
            equation_txt = 'Y=%0.3f*X-%0.3e' % (slope, np.absolute(intercept))
        corr_txt = 'Pearson r=%0.3f' % (corr)
        
        for i, txt in enumerate([equation_txt, txt2, corr_txt]):
            txt_label = Label(x=x_0, y=(x_1 - (i + 1)*delta), text=txt)
            p.add_layout(txt_label)

    if out_html is not None:
        title = out_html.split('.')[0]
        bokeh.io.save(p, filename=out_html, title=title)
    return p


def plot_lm(df, x_dim, y_dim, group_dim,
            opt_spec={'Overlay': {'plot': dict(legend_position='top_left')},
                      'Histogram': {'style': dict(width=200, height=400),
                                    'plot': dict(alpha=0.3)},
                      'Scatter': {'style': dict(width=500, height=400),
                                    'plot': dict(alpha=0.5)}}):
    
    import holoviews as hv
    
    x_dimension = hv.Dimension(x_dim[0], **x_dim[1])
    y_dimension = hv.Dimension(y_dim[0], **y_dim[1])
    scatters = {}
    lines = {}
    for group_name, group_label in group_dim[1]:
        dat = df.loc[df[group_dim[0]] == group_name][[x_dim[0], y_dim[0]]]
        scat = hv.Scatter(dat, x_dimension, y_dimension, label=group_label)
        x, y = dat[x_dim[0]], dat[y_dim[0]]
        xs = np.array([np.min(x), np.max(x)])
        slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
        ys = np.array([(slope * xs[0] + intercept), (slope * xs[1] + intercept)])
        txt = "Y = %s*X %s" % (str_format_val(slope), str_format_val(intercept, signed=True)) 
        line = hv.Curve((xs, ys), x_dimension, y_dimension, label=txt)

        scatters[group_name] = scat
        lines[group_name] = line
     
    return (scatters, lines)     
     

    
