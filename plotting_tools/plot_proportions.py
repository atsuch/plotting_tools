#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue 20 Mar 2018

@author: tsuchida

A simple tool to read proportional data and plot pie chart or stacked graph
"""

import pandas as pd

def main(dat, out_fname, kind='pie'):

    # read dat_txt
    if isinstance(dat, basestring):
        dat = pd.read_csv(dat, index_col=0)

    if kind == 'pie':
        p = dat.plot.pie(figsize=(6, 6), autopct='%0.2f')

    elif kind == 'bar':
        unstacked_dat = dat.unstack()
        p = unstacked_dat.plot.barh(stacked=True)

    fig = p.get_figure()
    fig.savefig(out_fname)

    return fig

if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Plot proportional data from txt and saves it')
    parser.add_argument('-d', '--dat', action='store',
                        required=True,
                        help='data txt containing the proportional data')
    parser.add_argument('-o', '--out_fname', action='store',
                        required=True,
                        help='output filename')
    parser.add_argument('-k', '--kind', action='store',
                        required=False, choices=['pie', 'bar'],
                        default='pie',
                        help='type of plot')

    args = vars(parser.parse_args())

    main(**args)
