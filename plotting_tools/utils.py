#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  8 18:30:48 2018

@author: tsuchida
"""
import numpy as np
import pandas as pd
import nibabel as nib
import matplotlib as plt
import matplotlib.cm as cm


def get_img_dat(img):
    """
    convenience fxn to get img data as numpy
    """
    return np.asanyarray(nib.load(img).dataobj) if isinstance(img, basestring) else np.asanyarray(img.dataobj)


def get_1d_dat(img, mask=None, l_thr=None, u_thr=None, nonzero=True, nonna=True):
    """
    Function to grab values from image data in img (NiftiImage or path to img)
    and retuns 1d array of the shape (num voxels, ).

    If a mask is provided, only values from voxels within the mask are extracted.
    
    If l_ or u_thr is provided, values below or above the values are discarded.

    If nonzero=True, only nonzero values are extracted, and if nonna=True,
    only non-na values are kept.

    """
    img_dat = get_img_dat(img)

    if mask is not None:
         mask_dat = get_img_dat(mask)
         dat = img_dat[mask_dat > 0]
    else:
        dat = img_dat.flatten()
    
    if nonna:
        dat = dat[~np.isnan(dat)]

    if nonzero:
        if not nonna:
            dat = dat[np.array([e != 0 if ~np.isnan(e) else False for e in dat], dtype=bool)]
        else:
            dat = dat[dat != 0]
    
    if l_thr is not None:
        dat = dat[dat > l_thr]
        
    if u_thr is not None:
        dat = dat[dat < u_thr]
        
    return dat


def summarize_img_intensity_distribution(img, 
                                         stats=['mean', 'std', 'median', 
                                                '10p', '25p', '75p', '90p',
                                                'min', 'max', 'count'],
                                         custom_func_dict=None,
                                         mask=None, l_thr=None, u_thr=None,
                                         nonzero=True, nonna=True):
    """
    Function to get a basic distribution stats summary from a img with or without mask.
    
    Available measures are;
    -mean and std
    -median
    -10th, 25th, 75th, 90th percentile
    -min and max
    -count of the voxels
    
    Returns a dict containing these summary measures.
    
    """

    flat_dat = get_1d_dat(img, mask=mask, l_thr=l_thr, u_thr=u_thr, nonzero=nonzero, nonna=nonna)
    
    summary_dat = {}
    def_dict = {'mean': lambda x: np.mean(x),
                'std': lambda x: np.std(x),
                'median': lambda x: np.median(x),
                '10p': lambda x: np.percentile(x, 10),
                '25p': lambda x: np.percentile(x, 25),
                '75p': lambda x: np.percentile(x, 75),
                '90p': lambda x: np.percentile(x, 90),
                'min': lambda x: np.min(x),
                'max': lambda x: np.max(x),
                'count': lambda x: x.shape[0]}
    
    # Add any custom func
    stats_dict = def_dict.copy()
    if custom_func_dict is not None:
        stats_dict.update(custom_func_dict)
        stats += custom_func_dict.keys()
        
    for measure in stats:
        summary_dat[measure] = stats_dict[measure](flat_dat)
   
    return summary_dat
        
    
def get_histmat_df(img_idx, im_list, bin_num=100, col_strform=None, density=False):
    """
    Function to get DataFrame containing a histogram values of images in im_list.

    The bins are determined by global distribution by taking 3 IQR +/- median.

    If density=True, histogram values are density of total voxels.

    Returns DataFrame of the length im_list/img_idx.
    """
    # Grab nonzero (and also non-NA) data from im_path
    nonzero_data = []
    for im in im_list:
        im_dat = get_1d_dat(im)
        nonzero_data.append(im_dat)
    stacked = np.hstack(nonzero_data)

    # Get the range of plotting
    # Here we define the range by median +/- 3 IQR of the global distribution.
    # Global bins are defined by taking the linspace between the range.
    iqr = (np.percentile(stacked, 75) - np.percentile(stacked, 25))
    (l, u) = ((np.median(stacked) - 3*iqr), (np.median(stacked) + 3*iqr))
    bins = np.linspace(l, u, bin_num)
    bins = np.insert(bins, 0, 0)
    bins = np.append(bins, np.max(stacked))

    # Get histmat
    hists = []
    for dat in nonzero_data:
        hist, bins = np.histogram(dat, bins=bins)
        # calculate rather than using density option since it may cause problems
        # when using unequal bins (for min and max bins)
        if density:
            hist = np.divide(hist, np.sum(hist, dtype=np.float))
        hists.append(hist)
    histmat =  np.vstack([h.reshape((1, h.shape[0])) for h in hists])

    # Use str-formatted bins for columns
    if col_strform is None:
        col_strform = "%0.1f" if np.absolute(np.min(l)) > 0.1 else "%0.1e"
    columns = [col_strform % val for val in bins][:-1]
    columns[0] = ("<" + col_strform ) % l
    columns[-1] = (">" + col_strform ) % u
    cols = pd.Index(columns, name="Image Intensity")

    histmat_df = pd.DataFrame(data=histmat, index=img_idx, columns=cols)

    return histmat_df


def get_hist_df(img_idx, im_list, bins=None, num_bins=100, bin_strform=None, density=False):
    """
    Function to get DataFrame containing a histogram values of images in im_list.

    If the bins are not given, it will use the num_bins and set the range to the
    approximate  3 IQR +/- median of the global distribution, computed by taking 
    the mean of median and IQR values for each img.

    If density=True, histogram values are density of total voxels.

    Returns DataFrame of the length im_list/img_idx.
    """
    # Get median and iqr from each img from im_path if bins are not provided 
    # to calculatethe bin
    if bins is None:
        stats_dict = {'median':[], 'iqr':[], 'min':[], 'max':[]}
        for im in im_list:
            im_summary = summarize_img_intensity_distribution( 
                            im, stats=['median', '25p', '75p', 'min', 'max'])
            stats_dict['median'].append(im_summary['median'])
            iqr = im_summary['75p'] - im_summary['25p']
            stats_dict['iqr'].append(iqr)
            stats_dict['min'].append(im_summary['min'])
            stats_dict['max'].append(im_summary['max'])
            
        # bins are calculated as the mean of median +/- mean of 3 IQR
        global_stats = {}
        for key, item in stats_dict.items():
            if key == 'max':
                global_stats[key] = np.max(np.asarray(item))
            elif key == 'min':
                global_stats[key] = np.min(np.asarray(item))
            else:
                global_stats[key] = np.mean(np.asarray(item))

        lower = global_stats['median'] - 3 * global_stats['iqr']
        upper = global_stats['median'] + 3 * global_stats['iqr']
        bins = np.linspace(lower, upper, num_bins)
        bins = np.insert(bins, 0, global_stats['min'])
        bins = np.append(bins, global_stats['max'])
 

    # Compute histogram values for each column in cols_to_plot and keep them in a df
    # For bins col, use str-formatted bins
    if bin_strform is None:
        bin_strform = "%0.1f" if np.absolute(np.min(global_stats['min'])) > 0.1 else "%0.1e"
    str_bins = ["-".join([bin_strform, bin_strform]) % (l, u) for l, u in zip(bins[:-1], bins[1:])]
    hist_df = pd.DataFrame({'bins': str_bins})
    for idx, img in zip(img_idx, im_list):
        dat = get_1d_dat(img)
        hist, bins = np.histogram(dat, bins=bins)
        # calculate rather than using density option since it may cause problems
        # when using unequal bins (for min and max bins)
        if density:
            hist = np.divide(hist, np.sum(hist, dtype=np.float))
        hist_df[idx] = hist
    return hist_df


def stack_df(summary_df, stack_cols, stacked_col_name, val_name):
    """
    Function to stack df
    """
    # index columns that should not be stacked
    indices = [col for col in summary_df.columns if col not in stack_cols]
    if len(indices) == 0:
        stacked = summary_df.stack().reset_index(level=1)
        stacked.columns = [stacked_col_name, val_name]
    else:
        indexed = summary_df.set_index(indices)
        stacked = indexed.stack().reset_index()
        stacked.columns = indices + [stacked_col_name, val_name]

    return stacked


def get_bokehpalette(cmap, N):
    """
    Function to create bokeh palette of size N

    cmap: any matplotlib colormap
    """
    colormap =cm.get_cmap(cmap, lut=N)
    bokehpalette = [plt.colors.rgb2hex(m) for m in colormap(np.arange(colormap.N))]

    return bokehpalette


def get_lims(arr, delta=0.01):
    max_val = np.nanmax(arr)
    min_val = np.nanmin(arr)
    margin = np.absolute((max_val - min_val))* delta
    upper = max_val + margin
    lower = min_val - margin
    return (lower, upper)


def str_format_val(val, num_digits=3, signed=False):
    """
    Function to return float or scientific format of the val.
    
    Returns float format if absolute(val) >= 0.1
    Returns scientifc format if absolute(val) < 0.1
    """
    
    if np.absolute(val) >= 0.1:
        return '{1:+0.{0:d}f}'.format(num_digits, val) if signed else '{1:0.{0:d}f}'.format(num_digits, val)
    
    else:
        return '{1:+0.{0:d}e}'.format(num_digits, val) if signed else '{1:0.{0:d}e}'.format(num_digits, val)