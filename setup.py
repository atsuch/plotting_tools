import pathlib
from setuptools import setup

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()

# This call to setup() does all the work
setup(
    name="plotting_tools",
    version="1.0.0",
    description="Useful tools for interactive plotting",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/atsuch/plotting_tools.git",
    author="Ami Tsuchida",
    author_email="atsuch@gmail.com",
    license="MIT",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
    ],
    packages=["plotting_tools"],
    include_package_data=True,
    install_requires=["numpy", "pandas", "scipy", ¨nibabel¨, "matplotlib", "seaborn", "bokeh"],

)